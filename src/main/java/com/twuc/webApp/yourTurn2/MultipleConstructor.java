package com.twuc.webApp.yourTurn2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Integer number;
    private Dependent dependent;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(Integer number, Dependent dependent) {
        this.number = number;
        this.dependent = dependent;
    }

    public MultipleConstructor(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
