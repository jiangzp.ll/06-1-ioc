package com.twuc.webApp.yourTurn2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;

    @Autowired
    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
    }

    @Autowired
    String initialize(AnotherDependent anotherDependent) {
        return anotherDependent.getInfo();
    }

    public Dependent getDependent() {
        return dependent;
    }
}
