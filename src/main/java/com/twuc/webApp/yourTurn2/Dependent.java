package com.twuc.webApp.yourTurn2;

import org.springframework.stereotype.Component;

@Component
public class Dependent {
    private String info;

    public Dependent() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
