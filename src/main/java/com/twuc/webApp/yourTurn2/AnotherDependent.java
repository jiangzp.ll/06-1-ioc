package com.twuc.webApp.yourTurn2;

import org.springframework.stereotype.Component;

@Component
public class AnotherDependent {
    private String info;

    public AnotherDependent() {
    }

    public String getInfo() {
        return "AnotherDependent";
    }
}
