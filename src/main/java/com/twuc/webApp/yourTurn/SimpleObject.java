package com.twuc.webApp.yourTurn;

public class SimpleObject implements SimpleInterface {

    private final String name;
    private final SimpleDependent simpleDependent;

    public SimpleObject(String name, SimpleDependent simpleDependent) {
        this.name = name;
        this.simpleDependent = simpleDependent;
    }

    public String getName() {
        return this.name;
    }



    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }
}
