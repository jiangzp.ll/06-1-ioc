package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class WithoutDependencyTest {

    AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");

    //2.1 AnnotationConfigApplicationContext的使用
    @Test
    void should_create_no_dependent_WithoutDependency_using_annotationConfig_application_context() {
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency);
        assertSame(WithoutDependency.class, withoutDependency.getClass());

    }

    @Test
    void should_create_dependent_WithoutDependency_using_annotationConfig_application_context() {
        Dependent dependent = context.getBean(Dependent.class);
        assertNotNull(dependent);
        assertSame(WithoutDependency.class, dependent.getClass());

    }

    @Test
    void should_test_OutOfScanningScope_no_Component() {
        assertThrows(RuntimeException.class,() -> {
            try {
                OutOfScanningScope outOfScanningScope = context.getBean(OutOfScanningScope.class);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        });
    }

    @Test
    void should_test_getBean_by_interface() {
        Interface myInterface = context.getBean(Interface.class);
//        assertSame(Interface.class, myInterface.getClass());
        assertNotNull(myInterface);
        assertSame(InterfaceImpl.class, myInterface.getClass());
    }

    @Test
    void should_use_Configuration_and_Bean_to_add_dependency() {
        SimpleObject simpleObject = (SimpleObject) context.getBean(SimpleInterface.class);
        SimpleDependent simpleDependent = simpleObject.getSimpleDependent();
        assertNotNull(simpleObject);
        assertNotNull(simpleDependent);
        assertEquals("O_o",simpleDependent.getName());
    }


}
