package com.twuc.webApp.yourTurn2;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class MultipleConstructorTest {

    AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn2");

    //2.2 AnnotationConfigApplicationContext其他用例
    @Test
    void should_use_AutoWired_with_constructor() {
        MultipleConstructor multipleConstructor = context.getBean(MultipleConstructor.class);
        multipleConstructor.getDependent().setInfo("O_o");
        assertEquals("O_o",multipleConstructor.getDependent().getInfo());
    }

    @Test
    void should_use_AutoWired_with_constructor_and_method() {
        WithAutowiredMethod withAutoWiredMethod = context.getBean(WithAutowiredMethod.class);
        assertEquals("AnotherDependent", withAutoWiredMethod.initialize(new AnotherDependent()));
        assertSame(Dependent.class, withAutoWiredMethod.getDependent().getClass());
    }

    @Test
    void name() {
        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Object[] sorted = beans.values().stream().map(
                (item) -> item.getClass().getName()
        ).sorted().toArray();
        assertEquals(3, sorted.length);
        assertEquals("com.twuc.webApp.yourTurn2.ImplementationA", sorted[0]);
        assertEquals("com.twuc.webApp.yourTurn2.ImplementationB", sorted[1]);
        assertEquals("com.twuc.webApp.yourTurn2.ImplementationC", sorted[2]);
    }
}
